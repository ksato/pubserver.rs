use actix_web::{
    http, middleware::Logger, web, App, HttpRequest, HttpResponse, HttpServer, Responder,
};
use bytes::Bytes;
use db_mongo::{BookOptions, DbClient};
use dotenv::dotenv;
use lz4::{Decoder, EncoderBuilder};
use redis::{Commands, RedisResult};
use regex::Regex;
use serde::Serialize;
use std::collections::hash_map::DefaultHasher;
use std::error::Error;
use std::hash::Hasher;
use std::io::{self, prelude::*, BufReader};

const API_ROOT: &str = "/api/v1";
const HOSTPORT: &str = "0.0.0.0:3000";

fn resp_as_json<T: Serialize>(value: T, if_none_match: String) -> HttpResponse {
    match serde_json::to_string(&value) {
        Ok(body) => {
            let mut hasher = DefaultHasher::new();
            hasher.write(body.as_bytes());
            let etag = hasher.finish().to_string();
            if etag == if_none_match {
                HttpResponse::NotModified()
                    .set_header(http::header::ETAG, etag)
                    .finish()
            } else {
                HttpResponse::Ok()
                    .content_type("application/json; charset=utf-8")
                    .set_header(http::header::ETAG, etag)
                    .body(body)
            }
        }
        Err(_err) => HttpResponse::InternalServerError().finish(),
    }
}

fn gen_etag(body: &Bytes) -> String {
    let mut hasher = DefaultHasher::new();
    hasher.write(body);
    hasher.finish().to_string()
}

fn if_none_match(req: &HttpRequest) -> String {
    match req.headers().get(http::header::IF_NONE_MATCH) {
        Some(value) => value.to_str().unwrap().to_string(),
        None => String::new(),
    }
}

fn charset(ext: &str) -> &str {
    if ext == "card" {
        "utf-8"
    } else {
        "shift_jis"
    }
}

pub async fn get_books(req: HttpRequest) -> impl Responder {
    let cl = DbClient::new().await.unwrap();

    match serde_qs::from_str::<BookOptions>(req.query_string()) {
        Ok(options) => match cl.find_books(Some(options)).await {
            Ok(books) => resp_as_json(books, if_none_match(&req)),
            Err(_) => HttpResponse::InternalServerError().finish(),
        },
        Err(err) => {
            println!("error: {:?}", err);
            HttpResponse::BadRequest().finish()
        }
    }
}

pub async fn get_book_by_id(req: HttpRequest, book_id: web::Path<u32>) -> impl Responder {
    // println!("get_book_by_id: {:?}", req);

    let cl = DbClient::new().await.unwrap();
    match cl.find_one_book(*book_id, None).await {
        Ok(book) => match book {
            Some(book) => resp_as_json(book, if_none_match(&req)),
            None => HttpResponse::NotFound().finish(),
        },
        Err(error) => {
            println!("ERRROR: {:?}", error);
            HttpResponse::InternalServerError().finish()
        }
    }
}
enum CacheResult {
    NotModified {
        etag: String,
    },
    Response {
        body: Bytes,
        etag: String,
        ctype: String,
    },
}

pub async fn get_zipped(cl: DbClient, book_id: u32) -> Result<Bytes, Box<dyn Error>> {
    let options = BookOptions {
        fields: Some(vec!["text_url".to_string()]),
        ..Default::default()
    };

    let book = cl.find_one_book(book_id, Some(options)).await?.unwrap();
    let ext_url = book.text_url.unwrap();

    let client = reqwest::Client::builder()
        .user_agent("Mozilla/5.0")
        .build()?;
    let zip_bytes: Bytes = client.get(&ext_url).send().await?.bytes().await?;
    let buf_reader = BufReader::new(io::Cursor::new(zip_bytes));
    let mut archive = zip::ZipArchive::new(buf_reader).unwrap();
    let mut file = archive.by_index(0).unwrap();
    // let bytes = file.size();
    let mut bytes: Vec<u8> = vec![];
    let _ = file.read_to_end(&mut bytes);
    Ok(Bytes::from(bytes))
}

pub async fn get_ogpcard(cl: DbClient, book_id: u32, ext: &str) -> Result<Bytes, Box<dyn Error>> {
    let options = BookOptions {
        fields: Some(vec![
            "card_url".to_string(),
            "html_url".to_string(),
            "title".to_string(),
            "authors".to_string(),
        ]),
        ..Default::default()
    };

    let book = cl.find_one_book(book_id, Some(options)).await?.unwrap();
    let ext_url = match ext {
        "card" => book.card_url,
        _ => book.html_url,
    }
    .unwrap();

    let client = reqwest::Client::builder()
        .user_agent("Mozilla/5.0")
        .build()?;

    Ok(client.get(&ext_url).send().await?.bytes().await?)
}

async fn get_from_cache(
    book_id: u32,
    ext: &str,
    if_none_match: String,
) -> Result<CacheResult, Box<dyn Error>> {
    let mut rc = redis::Client::open("redis://127.0.0.1:6379/")?.get_connection()?;
    let etag: RedisResult<String> = rc.get(&if_none_match);
    let key = format!("{}{}", book_id, ext);
    match etag {
        Ok(ckey) => {
            assert_eq!(ckey, key);
            Ok(CacheResult::NotModified {
                etag: if_none_match,
            })
        }
        Err(_) => {
            let body: Vec<u8> = rc.get(&key).unwrap();
            let body = if body.is_empty() {
                // println!("body miss!!");
                let cl = DbClient::new().await?;
                let body = match ext {
                    "txt" => get_zipped(cl, book_id).await?,
                    _ => get_ogpcard(cl, book_id, ext).await?,
                };
                let mut encoder = EncoderBuilder::new().level(4).build(vec![]).unwrap();
                io::copy(&mut io::Cursor::new(&body), &mut encoder).unwrap();
                let (compressed, _) = encoder.finish();
                let _: () = rc.set(&key, compressed)?;
                body
            } else {
                // println!("body hit!!");
                let compressed = io::Cursor::new(body);
                let mut decoder = Decoder::new(compressed).unwrap();
                let mut uncompressed = vec![];
                io::copy(&mut decoder, &mut uncompressed).unwrap();
                Bytes::from(uncompressed)
            };
            let etag = gen_etag(&body);
            let _: () = rc.set(&etag, &key)?;
            let ctype = match ext {
                "txt" => "text/plain; charset=shift_jis".to_string(),
                _ => format!("text/html; charset={}", charset(ext)),
            };
            Ok(CacheResult::Response { body, etag, ctype })
        }
    }
}

fn resp_as_html(body: Bytes, etag: &str, ctype: &str) -> HttpResponse {
    HttpResponse::Ok()
        .content_type(ctype)
        .set_header(http::header::ETAG, etag)
        .body(body)
}

async fn get_book_data(req: HttpRequest, book_id: u32, ext: &str) -> HttpResponse {
    match get_from_cache(book_id, ext, if_none_match(&req)).await {
        Ok(cache_result) => match cache_result {
            CacheResult::NotModified { etag } => HttpResponse::NotModified()
                .set_header(http::header::ETAG, etag)
                .finish(),
            CacheResult::Response { body, etag, ctype } => resp_as_html(body, &etag, &ctype),
        },
        Err(error) => {
            println!("ERRROR: {:?}", error);
            HttpResponse::InternalServerError().finish()
        }
    }
}

pub async fn get_book_card(req: HttpRequest, book_id: web::Path<u32>) -> impl Responder {
    get_book_data(req, *book_id, "card").await
}

pub async fn get_book_content(req: HttpRequest, book_id: web::Path<u32>) -> impl Responder {
    let cap = Regex::new(r"format=(\w+)")
        .unwrap()
        .captures(req.query_string());
    let ext = match cap {
        Some(cap) => cap.get(1).unwrap().as_str(),
        None => "html",
    }
    .to_string();
    get_book_data(req, *book_id, &ext).await
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    dotenv().ok();

    std::env::set_var("RUST_LOG", "actix_server=info,actix_web=info");
    env_logger::init();

    HttpServer::new(move || {
        App::new().wrap(Logger::new("%a %r %s %b")).service(
            web::scope(API_ROOT)
                .service(web::resource("/books").to(get_books))
                .service(web::resource("/books/{id}").to(get_book_by_id))
                .service(web::resource("/books/{id}/card").to(get_book_card))
                .service(web::resource("/books/{id}/content").to(get_book_content))
                .default_service(web::resource("").route(web::get().to(|_req: HttpRequest| {
                    // println!("default_service: req={:?}", req);
                    HttpResponse::NotFound().finish()
                }))),
        )
    })
    .bind(HOSTPORT)?
    .run()
    .await
}

#[cfg(test)]
mod tests {
    use super::*;
    use actix_http::body::{Body, ResponseBody};
    use actix_web::{
        dev::ServiceResponse,
        http::{header, StatusCode},
        test,
    };

    #[actix_rt::test]
    async fn test_a_book() {
        let mut app =
            test::init_service(App::new().service(web::resource("/books/{id}").to(get_book_by_id)))
                .await;

        let uri = "/books/125";
        let etag = "5275821767170190761";

        // normal case
        let req = test::TestRequest::with_uri(uri);
        let res: ServiceResponse = test::call_service(&mut app, req.to_request()).await;

        assert_eq!(res.status(), StatusCode::OK);
        assert_eq!(res.headers().get(header::ETAG).unwrap(), etag);
        assert_eq!(
            res.headers().get(header::CONTENT_TYPE).unwrap(),
            "application/json; charset=utf-8"
        );

        // with etag
        let req = test::TestRequest::with_uri(uri).header(header::IF_NONE_MATCH, etag);
        let res: ServiceResponse = test::call_service(&mut app, req.to_request()).await;

        assert_eq!(res.status(), StatusCode::NOT_MODIFIED);
        assert_eq!(res.headers().get(header::ETAG).unwrap(), etag);
        assert!(res.headers().get(header::CONTENT_TYPE).is_none());

        // 404 case
        let req = test::TestRequest::with_uri("/books/12345");
        let res: ServiceResponse = test::call_service(&mut app, req.to_request()).await;

        assert_eq!(res.status(), StatusCode::NOT_FOUND);
    }

    #[actix_rt::test]
    async fn test_books() {
        let mut app =
            test::init_service(App::new().service(web::resource("/books").to(get_books))).await;

        let uri = "/books";
        let etag = "14610703422225856072";

        let req = test::TestRequest::with_uri(uri);
        let res: ServiceResponse = test::call_service(&mut app, req.to_request()).await;

        assert_eq!(res.status(), StatusCode::OK);
        assert_eq!(res.headers().get(header::ETAG).unwrap(), etag);
        assert_eq!(
            res.headers().get(header::CONTENT_TYPE).unwrap(),
            "application/json; charset=utf-8"
        );

        let req = test::TestRequest::with_uri(uri).header(header::IF_NONE_MATCH, etag);
        let res: ServiceResponse = test::call_service(&mut app, req.to_request()).await;

        assert_eq!(res.status(), StatusCode::NOT_MODIFIED);
        assert_eq!(res.headers().get(header::ETAG).unwrap(), etag);
        assert!(res.headers().get(header::CONTENT_TYPE).is_none());
    }

    #[actix_rt::test]
    async fn test_books_by_title() {
        let mut app =
            test::init_service(App::new().service(web::resource("/books").to(get_books))).await;
        let qs = serde_urlencoded::to_string(&[("title", "吾輩は猫である")]).unwrap();
        let req = test::TestRequest::with_uri(&format!("/books?{}", qs));
        let res: ServiceResponse = test::call_service(&mut app, req.to_request()).await;

        assert_eq!(res.status(), StatusCode::OK);
        let mut res = res;
        match res.take_body() {
            ResponseBody::Body(b) => match b {
                Body::Bytes(bytes) => assert_eq!(bytes.len(), 1838),
                _ => assert!(false),
            },
            ResponseBody::Other(_) => assert!(false),
        }
    }

    #[actix_rt::test]
    async fn test_books_by_author() {
        let mut app =
            test::init_service(App::new().service(web::resource("/books").to(get_books))).await;
        let qs = serde_urlencoded::to_string(&[("author", "素木しづ")]).unwrap();
        let req = test::TestRequest::with_uri(&format!("/books?{}", qs));
        let res: ServiceResponse = test::call_service(&mut app, req.to_request()).await;

        assert_eq!(res.status(), StatusCode::OK);
        let mut res = res;
        match res.take_body() {
            ResponseBody::Body(b) => match b {
                Body::Bytes(bytes) => assert_eq!(bytes.len(), 17492),
                _ => assert!(false),
            },
            ResponseBody::Other(_) => assert!(false),
        }
    }

    #[actix_rt::test]
    async fn test_books_limited_fields() {
        let mut app =
            test::init_service(App::new().service(web::resource("/books").to(get_books))).await;
        let qs = serde_urlencoded::to_string(&[
            ("title", "鼻"),
            ("fields[0]", "title"),
            ("fields[1]", "release_date"),
        ])
        .unwrap();
        let req = test::TestRequest::with_uri(&format!("/books?{}", qs));
        let res: ServiceResponse = test::call_service(&mut app, req.to_request()).await;

        assert_eq!(res.status(), StatusCode::OK);
        let mut res = res;
        match res.take_body() {
            ResponseBody::Body(b) => match b {
                Body::Bytes(bytes) => assert_eq!(bytes.len(), 3187),
                _ => assert!(false),
            },
            ResponseBody::Other(_) => assert!(false),
        }
    }

    #[actix_rt::test]
    async fn test_book_limit_skip() {
        let mut app =
            test::init_service(App::new().service(web::resource("/books").to(get_books))).await;
        let qs = serde_urlencoded::to_string(&[
            ("title", "/花/"),
            ("limit", "20"),
            ("skip", "100"),
            ("sort[0][0]", "release_date"),
            ("sort[0][1]", "1"),
        ])
        .unwrap();
        // println!("{}", qs);
        let req = test::TestRequest::with_uri(&format!("/books?{}", qs));
        let res: ServiceResponse = test::call_service(&mut app, req.to_request()).await;

        assert_eq!(res.status(), StatusCode::OK);
        let mut res = res;
        match res.take_body() {
            ResponseBody::Body(b) => match b {
                Body::Bytes(bytes) => assert_eq!(bytes.len(), 32773),
                _ => assert!(false),
            },
            ResponseBody::Other(_) => assert!(false),
        }

        let qs = serde_urlencoded::to_string(&[
            ("title", "/花/"),
            ("limit", "10"),
            ("skip", "180"),
            ("sort[0][0]", "release_date"),
            ("sort[0][1]", "1"),
        ])
        .unwrap();
        // println!("{}", qs);
        let req = test::TestRequest::with_uri(&format!("/books?{}", qs));
        let res: ServiceResponse = test::call_service(&mut app, req.to_request()).await;

        assert_eq!(res.status(), StatusCode::OK);
        let mut res = res;
        match res.take_body() {
            ResponseBody::Body(b) => match b {
                Body::Bytes(bytes) => assert_eq!(bytes.len(), 16032),
                _ => assert!(false),
            },
            ResponseBody::Other(_) => assert!(false),
        }
    }

    #[actix_rt::test]
    async fn test_book_after() {
        let mut app =
            test::init_service(App::new().service(web::resource("/books").to(get_books))).await;
        let qs = serde_urlencoded::to_string(&[
            ("title", "/月/"),
            ("limit", "50"),
            ("sort[0][0]", "release_date"),
            ("sort[0][1]", "1"),
            ("after", "2009-01-01"),
        ])
        .unwrap();
        // println!("{}", qs);
        let req = test::TestRequest::with_uri(&format!("/books?{}", qs));
        let res: ServiceResponse = test::call_service(&mut app, req.to_request()).await;

        assert_eq!(res.status(), StatusCode::OK);
        let mut res = res;
        match res.take_body() {
            ResponseBody::Body(b) => match b {
                Body::Bytes(bytes) => assert_eq!(bytes.len(), 81612),
                _ => assert!(false),
            },
            ResponseBody::Other(_) => assert!(false),
        }
    }

    #[actix_rt::test]
    async fn test_book_card() {
        let mut app = test::init_service(
            App::new().service(web::resource("/books/{id}/card").to(get_book_card)),
        )
        .await;

        let etag = "16785309808766491916";
        let ctype = "text/html; charset=utf-8";
        let req = test::TestRequest::with_uri("/books/123/card");
        let res: ServiceResponse = test::call_service(&mut app, req.to_request()).await;

        assert_eq!(res.status(), StatusCode::OK);
        assert_eq!(res.headers().get(header::ETAG).unwrap(), etag);
        assert_eq!(res.headers().get(header::CONTENT_TYPE).unwrap(), ctype);

        let mut res = res;
        match res.take_body() {
            ResponseBody::Body(b) => match b {
                Body::Bytes(bytes) => assert_eq!(bytes.len(), 9184),
                _ => assert!(false),
            },
            ResponseBody::Other(_) => assert!(false),
        }

        let req =
            test::TestRequest::with_uri("/books/123/card").header(header::IF_NONE_MATCH, etag);

        let res: ServiceResponse = test::call_service(&mut app, req.to_request()).await;
        assert_eq!(res.status(), StatusCode::NOT_MODIFIED);
        assert_eq!(res.headers().get(header::ETAG).unwrap(), etag);
        assert!(res.headers().get(header::CONTENT_TYPE).is_none());
    }

    #[actix_rt::test]
    async fn test_book_html() {
        let mut app = test::init_service(
            App::new().service(web::resource("/books/{id}/content").to(get_book_content)),
        )
        .await;

        let etag = "10018916172923276418";
        let ctype = "text/html; charset=shift_jis";
        let clen = 16234;
        let req = test::TestRequest::with_uri("/books/123/content?format=html");
        let res: ServiceResponse = test::call_service(&mut app, req.to_request()).await;

        assert_eq!(res.status(), StatusCode::OK);
        assert_eq!(res.headers().get(header::ETAG).unwrap(), etag);
        assert_eq!(res.headers().get(header::CONTENT_TYPE).unwrap(), ctype);
        let mut res = res;
        match res.take_body() {
            ResponseBody::Body(b) => match b {
                Body::Bytes(bytes) => assert_eq!(bytes.len(), clen),
                _ => assert!(false),
            },
            ResponseBody::Other(_) => assert!(false),
        }

        // default to html
        let req = test::TestRequest::with_uri("/books/123/content");
        let res: ServiceResponse = test::call_service(&mut app, req.to_request()).await;

        assert_eq!(res.status(), StatusCode::OK);
        assert_eq!(res.headers().get(header::ETAG).unwrap(), etag);
        assert_eq!(res.headers().get(header::CONTENT_TYPE).unwrap(), ctype);
        let mut res = res;
        match res.take_body() {
            ResponseBody::Body(b) => match b {
                Body::Bytes(bytes) => assert_eq!(bytes.len(), clen),
                _ => assert!(false),
            },
            ResponseBody::Other(_) => assert!(false),
        }

        let req =
            test::TestRequest::with_uri("/books/123/content").header(header::IF_NONE_MATCH, etag);

        let res: ServiceResponse = test::call_service(&mut app, req.to_request()).await;
        assert_eq!(res.status(), StatusCode::NOT_MODIFIED);
        assert_eq!(res.headers().get(header::ETAG).unwrap(), etag);
        assert!(res.headers().get(header::CONTENT_TYPE).is_none());
    }

    #[actix_rt::test]
    async fn test_book_text() {
        let mut app = test::init_service(
            App::new().service(web::resource("/books/{id}/content").to(get_book_content)),
        )
        .await;

        let etag = "12079567175255201500";
        let ctype = "text/plain; charset=shift_jis";
        let clen = 10211;
        let req = test::TestRequest::with_uri("/books/123/content?format=txt");
        let res: ServiceResponse = test::call_service(&mut app, req.to_request()).await;

        assert_eq!(res.status(), StatusCode::OK);
        assert_eq!(res.headers().get(header::ETAG).unwrap(), etag);
        assert_eq!(res.headers().get(header::CONTENT_TYPE).unwrap(), ctype);
        let mut res = res;
        match res.take_body() {
            ResponseBody::Body(b) => match b {
                Body::Bytes(bytes) => assert_eq!(bytes.len(), clen),
                _ => assert!(false),
            },
            ResponseBody::Other(_) => assert!(false),
        }

        let req = test::TestRequest::with_uri("/books/123/content?format=txt")
            .header(header::IF_NONE_MATCH, etag);

        let res: ServiceResponse = test::call_service(&mut app, req.to_request()).await;
        assert_eq!(res.status(), StatusCode::NOT_MODIFIED);
        assert_eq!(res.headers().get(header::ETAG).unwrap(), etag);
        assert!(res.headers().get(header::CONTENT_TYPE).is_none());
    }
}
