use chrono::SecondsFormat;
use chrono::{DateTime, FixedOffset};
use serde::de::{Deserializer, Error, MapAccess, Visitor};
use serde::ser::Serializer;
use std::fmt;

#[derive(Debug, Clone, PartialEq)]
pub struct DateTimeUtc {
    dt: DateTime<FixedOffset>,
}

impl From<&str> for DateTimeUtc {
    fn from(s: &str) -> Self {
        Self {
            dt: DateTime::parse_from_rfc3339(s).unwrap(),
        }
    }
}
impl From<&DateTimeUtc> for String {
    fn from(dtu: &DateTimeUtc) -> String {
        dtu.dt.to_rfc3339_opts(SecondsFormat::Secs, true)
    }
}

pub fn serialize<S>(value: &Option<DateTimeUtc>, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    match value {
        Some(datetime) => serializer.serialize_str(&String::from(datetime)),
        None => serializer.serialize_none(),
    }
}

pub fn deserialize<'de, D>(deserializer: D) -> Result<Option<DateTimeUtc>, D::Error>
where
    D: Deserializer<'de>,
{
    struct TheVisitor;

    impl<'de> Visitor<'de> for TheVisitor {
        type Value = Option<DateTimeUtc>;

        fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
            formatter.write_str("format error")
        }

        fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
        where
            E: Error,
        {
            Ok(Some(DateTimeUtc::from(v)))
        }

        fn visit_unit<E>(self) -> Result<Self::Value, E>
        where
            E: Error,
        {
            Ok(None)
        }

        fn visit_map<A>(self, map: A) -> Result<Self::Value, A::Error>
        where
            A: MapAccess<'de>,
        {
            let mut map = map;
            let entry = map.next_entry::<String, String>().unwrap().unwrap();
            Ok(Some(DateTimeUtc::from(entry.1.as_str())))
        }
    }

    deserializer.deserialize_any(TheVisitor)
}
