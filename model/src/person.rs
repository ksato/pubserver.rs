use derive_builder::Builder;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, PartialEq, Debug, Default, Builder)]
#[builder(setter(strip_option, into), default)]
pub struct Person {
    pub person_id: Option<i32>,
    pub last_name: Option<String>,
    pub first_name: Option<String>,
    pub last_name_yomi: Option<String>,
    pub first_name_yomi: Option<String>,
    pub last_name_sort: Option<String>,
    pub first_name_sort: Option<String>,
    pub last_name_roman: Option<String>,
    pub first_name_roman: Option<String>,
    pub date_of_birth: Option<String>,
    pub date_of_death: Option<String>,
    pub author_copyright: Option<bool>,
}

#[cfg(test)]
mod tests {
    use super::*;
    use serde_json;

    #[test]
    fn person_struct_creation() {
        let person: Person = PersonBuilder::default()
            .person_id(957)
            .last_name("鈴木")
            .first_name("梅太郎")
            .last_name_yomi("すずき")
            .first_name_yomi("うめたろう")
            .last_name_sort("すすき")
            .first_name_sort("うめたろう")
            .last_name_roman("Suzuki")
            .first_name_roman("Umetaro")
            .date_of_birth("1874-04-07")
            .date_of_death("1943-09-20")
            .author_copyright(false)
            .build()
            .unwrap();

        // println!("{}", serde_json::to_string(&book).unwrap());
        let serialized = r#"{"person_id":957,"last_name":"鈴木","first_name":"梅太郎","last_name_yomi":"すずき","first_name_yomi":"うめたろう","last_name_sort":"すすき","first_name_sort":"うめたろう","last_name_roman":"Suzuki","first_name_roman":"Umetaro","date_of_birth":"1874-04-07","date_of_death":"1943-09-20","author_copyright":false}"#;
        assert_eq!(serde_json::to_string(&person).unwrap(), serialized);

        let person2: Person = serde_json::from_str(&serialized).unwrap();
        assert_eq!(person, person2);
    }

    #[test]
    fn default_person() {
        let person: Person = PersonBuilder::default().person_id(123).build().unwrap();
        let serialized = r#"{"person_id":123,"last_name":null,"first_name":null,"last_name_yomi":null,"first_name_yomi":null,"last_name_sort":null,"first_name_sort":null,"last_name_roman":null,"first_name_roman":null,"date_of_birth":null,"date_of_death":null,"author_copyright":null}"#;
        let person2: Person = serde_json::from_str(&serialized).unwrap();
        assert_eq!(person, person2);
    }
}
