use crate::date_convert::{self, DateTimeUtc};
use derive_builder::Builder;
use serde::{Deserialize, Serialize};
use serde_with::skip_serializing_none;

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub struct PersonSummary {
    pub person_id: i32,
    pub last_name: String,
    pub first_name: String,
}

impl PersonSummary {
    pub fn new(person_id: i32, last_name: &str, first_name: &str) -> Self {
        PersonSummary {
            person_id,
            last_name: last_name.to_string(),
            first_name: first_name.to_string(),
        }
    }
}

#[skip_serializing_none]
#[derive(Serialize, Deserialize, PartialEq, Debug, Default, Builder)]
#[builder(setter(strip_option, into), default)]
#[serde(default)]
pub struct Book {
    pub book_id: Option<i32>,
    pub title: Option<String>,
    pub title_yomi: Option<String>,
    pub title_sort: Option<String>,
    pub subtitle: Option<String>,
    pub subtitle_yomi: Option<String>,
    pub original_title: Option<String>,
    pub first_appearance: Option<String>,
    pub ndc_code: Option<String>,
    pub font_kana_type: Option<String>,
    pub copyright: Option<bool>,
    #[serde(with = "date_convert")]
    pub release_date: Option<DateTimeUtc>,
    #[serde(with = "date_convert")]
    pub last_modified: Option<DateTimeUtc>,
    pub card_url: Option<String>,
    pub base_book_1: Option<String>,
    pub base_book_1_publisher: Option<String>,
    pub base_book_1_1st_edition: Option<String>,
    pub base_book_1_edition_input: Option<String>,
    pub base_book_1_edition_proofing: Option<String>,
    pub base_book_1_parent: Option<String>,
    pub base_book_1_parent_publisher: Option<String>,
    pub base_book_1_parent_1st_edition: Option<String>,
    pub base_book_2: Option<String>,
    pub base_book_2_publisher: Option<String>,
    pub base_book_2_1st_edition: Option<String>,
    pub base_book_2_edition_input: Option<String>,
    pub base_book_2_edition_proofing: Option<String>,
    pub base_book_2_parent: Option<String>,
    pub base_book_2_parent_publisher: Option<String>,
    pub base_book_2_parent_1st_edition: Option<String>,
    pub input: Option<String>,
    pub proofing: Option<String>,
    pub text_url: Option<String>,
    #[serde(with = "date_convert")]
    pub text_last_modified: Option<DateTimeUtc>,
    pub text_encoding: Option<String>,
    pub text_charset: Option<String>,
    pub text_updated: Option<i32>,
    pub html_url: Option<String>,
    #[serde(with = "date_convert")]
    pub html_last_modified: Option<DateTimeUtc>,
    pub html_encoding: Option<String>,
    pub html_charset: Option<String>,
    pub html_updated: Option<i32>,
    pub revisers: Option<Vec<PersonSummary>>,
    pub translators: Option<Vec<PersonSummary>>,
    pub authors: Option<Vec<PersonSummary>>,
}

impl Book {
    pub fn default_builder() -> BookBuilder {
        BookBuilder::default()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use serde_json;

    #[test]
    fn person_summary_struct_creation() {
        let person_summary = PersonSummary {
            person_id: 321,
            last_name: "MyLastName".to_string(),
            first_name: "MyFirstName".to_string(),
        };
        assert_eq!(person_summary.person_id, 321);
        assert_eq!(person_summary.last_name, "MyLastName");
        assert_eq!(person_summary.first_name, "MyFirstName");

        let serialized =
            "{\"person_id\":321,\"last_name\":\"MyLastName\",\"first_name\":\"MyFirstName\"}";
        assert_eq!(serde_json::to_string(&person_summary).unwrap(), serialized);

        let person_summary2: PersonSummary = serde_json::from_str(&serialized).unwrap();
        assert_eq!(person_summary2.person_id, 321);
        assert_eq!(person_summary2.last_name, "MyLastName");
        assert_eq!(person_summary2.first_name, "MyFirstName");
    }

    #[test]
    fn book_struct() {
        let book: Book = Book::default_builder()
            .book_id(123)
            .title("大川の水")
            .title_yomi("おおかわのみず")
            .title_sort("おおかわのみす")
            .subtitle("")
            .subtitle_yomi("")
            .original_title("")
            .first_appearance("「心の花」1914（大正3）年4月")
            .ndc_code("NDC 914")
            .font_kana_type("新字新仮名")
            .copyright(false)
            .release_date("1999-01-11T00:00:00Z")
            .last_modified("2014-09-17T00:00:00Z")
            .card_url("https://www.aozora.gr.jp/cards/000879/card123.html")
            .base_book_1("羅生門・鼻・芋粥")
            .base_book_1_publisher("角川文庫、角川書店")
            .base_book_1_1st_edition("1950（昭和25）年10月20日")
            .base_book_1_edition_input("1985（昭和60）年11月10日改版38版")
            .base_book_1_edition_proofing("1985（昭和60）年11月10日改版38版")
            .base_book_1_parent("")
            .base_book_1_parent_publisher("")
            .base_book_1_parent_1st_edition("")
            .base_book_2("")
            .base_book_2_publisher("")
            .base_book_2_1st_edition("")
            .base_book_2_edition_input("")
            .base_book_2_edition_proofing("")
            .base_book_2_parent("")
            .base_book_2_parent_publisher("")
            .base_book_2_parent_1st_edition("")
            .input("j.utiyama")
            .proofing("かとうかおり")
            .text_url("https://www.aozora.gr.jp/cards/000879/files/123_ruby_1199.zip")
            .text_last_modified("2004-03-15T00:00:00Z")
            .text_encoding("ShiftJIS")
            .text_charset("JIS X 0208")
            .text_updated(2)
            .html_url("https://www.aozora.gr.jp/cards/000879/files/123_15167.html")
            .html_last_modified("2004-03-15T00:00:00Z")
            .html_encoding("ShiftJIS")
            .html_charset("JIS X 0208")
            .html_updated(0)
            .authors(vec![PersonSummary::new(879, "芥川", "竜之介")])
            .build()
            .unwrap();

        // println!("{}", serde_json::to_string(&book).unwrap());
        let serialized = r#"{"book_id":123,"title":"大川の水","title_yomi":"おおかわのみず","title_sort":"おおかわのみす","subtitle":"","subtitle_yomi":"","original_title":"","first_appearance":"「心の花」1914（大正3）年4月","ndc_code":"NDC 914","font_kana_type":"新字新仮名","copyright":false,"release_date":"1999-01-11T00:00:00Z","last_modified":"2014-09-17T00:00:00Z","card_url":"https://www.aozora.gr.jp/cards/000879/card123.html","base_book_1":"羅生門・鼻・芋粥","base_book_1_publisher":"角川文庫、角川書店","base_book_1_1st_edition":"1950（昭和25）年10月20日","base_book_1_edition_input":"1985（昭和60）年11月10日改版38版","base_book_1_edition_proofing":"1985（昭和60）年11月10日改版38版","base_book_1_parent":"","base_book_1_parent_publisher":"","base_book_1_parent_1st_edition":"","base_book_2":"","base_book_2_publisher":"","base_book_2_1st_edition":"","base_book_2_edition_input":"","base_book_2_edition_proofing":"","base_book_2_parent":"","base_book_2_parent_publisher":"","base_book_2_parent_1st_edition":"","input":"j.utiyama","proofing":"かとうかおり","text_url":"https://www.aozora.gr.jp/cards/000879/files/123_ruby_1199.zip","text_last_modified":"2004-03-15T00:00:00Z","text_encoding":"ShiftJIS","text_charset":"JIS X 0208","text_updated":2,"html_url":"https://www.aozora.gr.jp/cards/000879/files/123_15167.html","html_last_modified":"2004-03-15T00:00:00Z","html_encoding":"ShiftJIS","html_charset":"JIS X 0208","html_updated":0,"authors":[{"person_id":879,"last_name":"芥川","first_name":"竜之介"}]}"#;
        assert_eq!(serde_json::to_string(&book).unwrap(), serialized);

        let book2: Book = serde_json::from_str(&serialized).unwrap();
        assert_eq!(book, book2);
    }
    #[test]
    fn book_struct_54322() {
        let book: Book = Book::default_builder()
            .book_id(54333)
            .title("食品の混ぜ物処理および調理の毒物（1820）")
            .title_yomi(
                "しょくひんのまぜものしょりおよびちょうりのどくぶつ（せんはっぴゃくにじゅう）",
            )
            .title_sort("しよくひんのませものしよりおよひちようりのとくふつせんはつひやくにしゆう")
            .subtitle("")
            .subtitle_yomi("")
            .original_title("A TREATISE ON ADULTERATIONS OF FOOD,\nAND CULINARY POISONS (1820)")
            .first_appearance("")
            .ndc_code("NDC 588")
            .font_kana_type("新字新仮名")
            .copyright(true)
            .release_date("2011-11-21T00:00:00Z")
            .last_modified("2014-09-16T00:00:00Z")
            .card_url("https://www.aozora.gr.jp/cards/001657/card54333.html")
            .base_book_1("")
            .base_book_1_publisher("")
            .base_book_1_1st_edition("")
            .base_book_1_edition_input("")
            .base_book_1_edition_proofing("")
            .base_book_1_parent("")
            .base_book_1_parent_publisher("")
            .base_book_1_parent_1st_edition("")
            .base_book_2("")
            .base_book_2_publisher("")
            .base_book_2_1st_edition("")
            .base_book_2_edition_input("")
            .base_book_2_edition_proofing("")
            .base_book_2_parent("")
            .base_book_2_parent_publisher("")
            .base_book_2_parent_1st_edition("")
            .input("")
            .proofing("")
            .text_url("")
            .text_encoding("")
            .text_charset("")
            .text_updated(-1)
            .html_url("http://www.geocities.jp/minakami30jp/accum/jaccum.html")
            .html_last_modified("2011-11-12T00:00:00Z")
            .html_encoding("ShiftJIS")
            .html_charset("JIS X 0208")
            .html_updated(0)
            .authors(vec![PersonSummary::new(1657, "アークム", "フレデリック")])
            .translators(vec![PersonSummary::new(1494, "水上", "茂樹")])
            .build()
            .unwrap();

        let serialized = r#"{"book_id":54333,"title":"食品の混ぜ物処理および調理の毒物（1820）","title_yomi":"しょくひんのまぜものしょりおよびちょうりのどくぶつ（せんはっぴゃくにじゅう）","title_sort":"しよくひんのませものしよりおよひちようりのとくふつせんはつひやくにしゆう","subtitle":"","subtitle_yomi":"","original_title":"A TREATISE ON ADULTERATIONS OF FOOD,\nAND CULINARY POISONS (1820)","first_appearance":"","ndc_code":"NDC 588","font_kana_type":"新字新仮名","copyright":true,"release_date":"2011-11-21T00:00:00Z","last_modified":"2014-09-16T00:00:00Z","card_url":"https://www.aozora.gr.jp/cards/001657/card54333.html","base_book_1":"","base_book_1_publisher":"","base_book_1_1st_edition":"","base_book_1_edition_input":"","base_book_1_edition_proofing":"","base_book_1_parent":"","base_book_1_parent_publisher":"","base_book_1_parent_1st_edition":"","base_book_2":"","base_book_2_publisher":"","base_book_2_1st_edition":"","base_book_2_edition_input":"","base_book_2_edition_proofing":"","base_book_2_parent":"","base_book_2_parent_publisher":"","base_book_2_parent_1st_edition":"","input":"","proofing":"","text_url":"","text_encoding":"","text_charset":"","text_updated":-1,"html_url":"http://www.geocities.jp/minakami30jp/accum/jaccum.html","html_last_modified":"2011-11-12T00:00:00Z","html_encoding":"ShiftJIS","html_charset":"JIS X 0208","html_updated":0,"translators":[{"person_id":1494,"last_name":"水上","first_name":"茂樹"}],"authors":[{"person_id":1657,"last_name":"アークム","first_name":"フレデリック"}]}"#;
        assert_eq!(serde_json::to_string(&book).unwrap(), serialized);

        let book2: Book = serde_json::from_str(&serialized).unwrap();
        assert_eq!(book, book2);
    }

    #[test]
    fn default_book() {
        let book_default: Book = Default::default();
        let serialized = r#"{"book_id":null,"title":null,"title_yomi":null,"title_sort":null,"subtitle":null,"subtitle_yomi":null,"original_title":null,"first_appearance":null,"ndc_code":null,"font_kana_type":null,"copyright":null,"release_date":null,"last_modified":null,"card_url":null,"base_book_1":null,"base_book_1_publisher":null,"base_book_1_1st_edition":null,"base_book_1_edition_input":null,"base_book_1_edition_proofing":null,"base_book_1_parent":null,"base_book_1_parent_publisher":null,"base_book_1_parent_1st_edition":null,"base_book_2":null,"base_book_2_publisher":null,"base_book_2_1st_edition":null,"base_book_2_edition_input":null,"base_book_2_edition_proofing":null,"base_book_2_parent":null,"base_book_2_parent_publisher":null,"base_book_2_parent_1st_edition":null,"input":null,"proofing":null,"text_url":null,"text_last_modified":null,"text_encoding":null,"text_charset":null,"text_updated":null,"html_url":null,"html_last_modified":null,"html_encoding":null,"html_charset":null,"html_updated":null,"revisers":null,"translators":null,"authors":null}"#;
        let book: Book = serde_json::from_str(&serialized).unwrap();
        assert_eq!(book_default, book);
        let serialized2 = "{}";
        let book2: Book = serde_json::from_str(&serialized2).unwrap();
        assert_eq!(book_default, book2);
    }

    #[test]
    fn null_book_to_json() {
        let null_book: Book = Default::default();
        assert_eq!(serde_json::to_string(&null_book).unwrap(), "{}");
    }
}
