use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, Default)]
pub struct BookOptions {
    pub title: Option<String>,
    pub author: Option<String>,
    pub fields: Option<Vec<String>>,
    pub limit: Option<i64>,
    pub skip: Option<i64>,
    pub sort: Option<Vec<(String, i32)>>,
    pub after: Option<String>,
}

#[cfg(test)]
mod tests {
    use super::*;
    use serde_qs as qs;
    #[test]
    fn test_decode() {
        let params = "title=%E5%90%BE%E8%BC%A9%E3%81%AF%E7%8C%AB%E3%81%A7%E3%81%82%E3%82%8B&author=%E5%A4%8F%E7%9B%AE%E6%BC%B1%E7%9F%B3";
        let book_options = qs::from_str::<BookOptions>(params).unwrap();
        assert_eq!(book_options.title.unwrap(), "吾輩は猫である");
        assert_eq!(book_options.author.unwrap(), "夏目漱石");

        let params = "author=%E7%8B%AC%E6%AD%A9";
        let book_options = qs::from_str::<BookOptions>(params).unwrap();
        assert!(book_options.title.is_none());
        assert_eq!(book_options.author.unwrap(), "独歩");

        let params = "title=%E9%BC%BB&fields[0]=title&fields[1]=release_date";
        let book_options = qs::from_str::<BookOptions>(params).unwrap();
        assert_eq!(book_options.title.unwrap(), "鼻");
        assert_eq!(book_options.fields.unwrap(), vec!["title", "release_date"]);

        // let mut book_options: BookOptions = Default::default();
        // book_options.sort = Some(vec![("release_date".to_string(), 1)]);
        // println!("{}", qs::to_string(&book_options).unwrap());

        let params = "limit=30&skip=50&sort[0][0]=release_date&sort[0][1]=1";
        let book_options = qs::from_str::<BookOptions>(params).unwrap();
        assert_eq!(book_options.limit.unwrap(), 30);
        assert_eq!(book_options.skip.unwrap(), 50);
        assert_eq!(
            book_options.sort.unwrap(),
            vec![("release_date".to_string(), 1)]
        );
    }
}
