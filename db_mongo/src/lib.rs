use bson::{doc, Bson, Document, Regex as BsonRegex};
use chrono::{DateTime, NaiveDate, Utc};
use futures::StreamExt;
use model::{Book, Person};
use mongodb::options::{ClientOptions, FindOneOptions, FindOptions};
use mongodb::{error, Client, Collection, Cursor, Database};
use regex::Regex;
use std::env;
use std::error::Error;

mod options;
pub use options::BookOptions;

#[derive(Clone, Debug)]
pub struct DbClient {
    client: Client,
}

impl DbClient {
    pub async fn new() -> Result<Self, Box<dyn Error>> {
        let mongo_url = format!(
            "mongodb://{}{}:{}/",
            env::var("AOZORA_MONGODB_CREDENTIAL").unwrap_or_default(),
            env::var("AOZORA_MONGODB_HOST").unwrap_or_else(|_| "localhost".to_string()),
            env::var("AOZORA_MONGODB_PORT").unwrap_or_else(|_| "27017".to_string()),
        );
        let client_options = ClientOptions::parse(&mongo_url).await?;
        Ok(Self {
            client: Client::with_options(client_options)?,
        })
    }

    fn _db(self) -> Database {
        self.client.database("aozora")
    }

    async fn _find(
        collection: Collection,
        filter: Document,
        options: FindOptions,
    ) -> error::Result<Cursor> {
        let mut options = options;
        if options.limit.is_none() {
            options.limit = Some(100)
        }
        let mut projection = options.projection.unwrap_or_default();
        projection.insert("_id", 0);
        options.projection = Some(projection);
        Ok(collection.find(filter, options).await?)
    }

    async fn _find_one(
        collection: Collection,
        filter: Option<Document>,
        options: Option<FindOneOptions>,
    ) -> error::Result<Option<Document>> {
        let mut options: FindOneOptions = options.unwrap_or_default();
        let mut projection = options.projection.unwrap_or_default();
        projection.insert("_id", 0);
        options.projection = Some(projection);
        Ok(collection.find_one(filter, options).await?)
    }

    pub async fn find_persons(self, name: String) -> Result<Vec<Person>, Box<dyn Error>> {
        let persons = self._db().collection("persons");
        let filter = doc! {
            "$where": format!("var p = \"{}\"; this.last_name + this.first_name == p || this.last_name == p || this.first_name == p", name)
        };
        // println!("filter: {:}", filter);
        let options: FindOptions = Default::default();
        let cursor = Self::_find(persons, filter, options).await?;
        let res = cursor.map(|doc| {
            let doc = doc.unwrap();
            // println!("{:?}", doc);
            let serialized = bson::from_bson(bson::Bson::Document(doc));
            // println!("{:?}", serialized);
            serialized.unwrap()
        });
        Ok(res.collect::<Vec<Person>>().await)
    }

    pub async fn find_books(
        self,
        book_options: Option<BookOptions>,
    ) -> Result<Vec<Book>, Box<dyn Error>> {
        let books = self.clone()._db().collection("books");
        let mut filter = Document::new();
        let mut options: FindOptions = Default::default();
        if let Some(book_options) = book_options {
            if let Some(title) = book_options.title {
                let title = match Regex::new(r"/(\w+)/").unwrap().captures(&title) {
                    Some(cap) => Bson::RegularExpression(BsonRegex {
                        pattern: cap.get(1).unwrap().as_str().to_string(),
                        options: String::new(),
                    }),
                    None => Bson::String(title),
                };
                filter.insert("title", title);
            }
            if let Some(author) = book_options.author {
                let persons = self.find_persons(author).await?;
                // println!("{:?}", persons);
                if persons.is_empty() {
                    return Ok(Vec::new());
                }
                let pids: Vec<i32> = persons.into_iter().map(|p| p.person_id.unwrap()).collect();
                // println!("pids={:?}", pids);
                filter.insert("authors.person_id", doc! { "$in" : pids });
            }
            // println!("fields: {:?}", book_options.fields);
            if let Some(fields) = book_options.fields {
                if !fields.is_empty() {
                    options.projection =
                        Some(fields.into_iter().map(|f| (f, Bson::from(1))).collect());
                }
            }
            if let Some(limit) = book_options.limit {
                options.limit = Some(limit);
            }
            if let Some(skip) = book_options.skip {
                options.skip = Some(skip);
            }
            if let Some(sort) = book_options.sort {
                options.sort = Some(sort.into_iter().map(|i| (i.0, Bson::from(i.1))).collect());
            }
            if let Some(after) = book_options.after {
                let after = NaiveDate::parse_from_str(&after, "%Y-%m-%d")
                    .unwrap()
                    .and_hms(0, 0, 0);
                let after = DateTime::<Utc>::from_utc(after, Utc);
                filter.insert("release_date", doc! { "$gte": after });
            }
        }
        // println!("filter: {:?}", filter);
        let cursor = Self::_find(books, filter, options).await?;
        let res = cursor.map(|doc| {
            let doc = doc.unwrap();
            // println!("doc ==> {:?}", doc);
            bson::from_bson(bson::Bson::Document(doc)).unwrap()
        });
        Ok(res.collect::<Vec<Book>>().await)
    }

    pub async fn find_one_book(
        self,
        book_id: u32,
        book_options: Option<BookOptions>,
    ) -> Result<Option<Book>, Box<dyn Error>> {
        let books = self._db().collection("books");
        let filter = Some(doc! { "book_id": book_id });
        let mut options: FindOneOptions = Default::default();
        if let Some(book_options) = book_options {
            // println!("fields: {:?}", book_options.fields);
            if let Some(fields) = book_options.fields {
                if !fields.is_empty() {
                    options.projection =
                        Some(fields.into_iter().map(|f| (f, Bson::from(1))).collect());
                }
            }
        }
        // println!("options: {:?}", options);
        Ok(match Self::_find_one(books, filter, Some(options)).await? {
            Some(document) => bson::from_bson(bson::Bson::Document(document)).unwrap(),
            None => None,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use actix_rt;
    use model::PersonSummary;

    #[actix_rt::test]
    async fn test_create_dbclient() {
        let dbclient = DbClient::new().await;
        // println!("{:#?}", dbclient);
        assert!(dbclient.is_ok());
        let db_names = dbclient
            .unwrap()
            .client
            .list_database_names(None, None)
            .await;
        assert!(db_names.is_ok());
        assert!(db_names.unwrap().contains(&"aozora".to_string()));
    }

    #[actix_rt::test]
    async fn test_find_books() {
        let dbclient = DbClient::new().await.unwrap();
        let books = dbclient.find_books(None).await.unwrap();
        assert_eq!(books.len(), 100)
    }

    #[actix_rt::test]
    async fn test_find_one_book() {
        let dbclient = DbClient::new().await.unwrap();
        let book = dbclient.find_one_book(123, None).await.unwrap().unwrap();
        let book_expected: Book = Book::default_builder()
            .book_id(123)
            .title("大川の水")
            .title_yomi("おおかわのみず")
            .title_sort("おおかわのみす")
            .subtitle("")
            .subtitle_yomi("")
            .original_title("")
            .first_appearance("「心の花」1914（大正3）年4月")
            .ndc_code("NDC 914")
            .font_kana_type("新字新仮名")
            .copyright(false)
            .release_date("1999-01-11T00:00:00Z")
            .last_modified("2014-09-17T00:00:00Z")
            .card_url("https://www.aozora.gr.jp/cards/000879/card123.html")
            .base_book_1("羅生門・鼻・芋粥")
            .base_book_1_publisher("角川文庫、角川書店")
            .base_book_1_1st_edition("1950（昭和25）年10月20日")
            .base_book_1_edition_input("1985（昭和60）年11月10日改版38版")
            .base_book_1_edition_proofing("1985（昭和60）年11月10日改版38版")
            .base_book_1_parent("")
            .base_book_1_parent_publisher("")
            .base_book_1_parent_1st_edition("")
            .base_book_2("")
            .base_book_2_publisher("")
            .base_book_2_1st_edition("")
            .base_book_2_edition_input("")
            .base_book_2_edition_proofing("")
            .base_book_2_parent("")
            .base_book_2_parent_publisher("")
            .base_book_2_parent_1st_edition("")
            .input("j.utiyama")
            .proofing("かとうかおり")
            .text_url("https://www.aozora.gr.jp/cards/000879/files/123_ruby_1199.zip")
            .text_last_modified("2004-03-15T00:00:00Z")
            .text_encoding("ShiftJIS")
            .text_charset("JIS X 0208")
            .text_updated(2)
            .html_url("https://www.aozora.gr.jp/cards/000879/files/123_15167.html")
            .html_last_modified("2004-03-15T00:00:00Z")
            .html_encoding("ShiftJIS")
            .html_charset("JIS X 0208")
            .html_updated(0)
            .authors(vec![PersonSummary::new(879, "芥川", "竜之介")])
            .build()
            .unwrap();

        assert_eq!(book, book_expected);

        {
            let dbclient = DbClient::new().await.unwrap();
            let book_not_found = dbclient.find_one_book(12345, None).await.unwrap();
            assert!(book_not_found.is_none())
        }
    }

    #[actix_rt::test]
    async fn search_by_title() {
        let dbclient = DbClient::new().await.unwrap();
        let options = BookOptions {
            title: Some("吾輩は猫である".to_string()),
            ..Default::default()
        };
        let books = dbclient.find_books(Some(options)).await.unwrap();
        assert_eq!(books.len(), 1);
        assert_eq!(books[0].book_id.unwrap(), 789);
        assert_eq!(books[0].title.as_ref().unwrap(), "吾輩は猫である");

        let dbclient = DbClient::new().await.unwrap();
        let options = BookOptions {
            title: Some("あいびき".to_string()),
            ..Default::default()
        };
        let books = dbclient.find_books(Some(options)).await.unwrap();
        assert_eq!(books.len(), 2);
        assert_eq!(books[0].book_id.unwrap(), 5);
        assert_eq!(books[1].book_id.unwrap(), 4843);
    }

    #[actix_rt::test]
    async fn search_by_regex_title() {
        let dbclient = DbClient::new().await.unwrap();
        let options = BookOptions {
            title: Some(r"/航海/".to_string()),
            ..Default::default()
        };
        let books = dbclient.find_books(Some(options)).await.unwrap();
        assert_eq!(books.len(), 3);
        assert_eq!(books[0].book_id.unwrap(), 47424);
        assert_eq!(books[0].title.as_ref().unwrap(), "死の航海");
        assert_eq!(books[1].book_id.unwrap(), 55525);
        assert_eq!(books[1].title.as_ref().unwrap(), "航海");
        assert_eq!(books[2].book_id.unwrap(), 51865);
        assert_eq!(books[2].title.as_ref().unwrap(), "軍艦金剛航海記");
    }

    #[actix_rt::test]
    async fn search_by_author() {
        // fullname
        let dbclient = DbClient::new().await.unwrap();
        let options = BookOptions {
            author: Some("素木しづ".to_string()),
            ..Default::default()
        };
        let books = dbclient.find_books(Some(options)).await.unwrap();
        assert_eq!(books.len(), 12);
        assert_eq!(books[0].book_id.unwrap(), 198);
        assert_eq!(books[0].title.as_ref().unwrap(), "青白き夢");

        // lastname
        let dbclient = DbClient::new().await.unwrap();
        let options = BookOptions {
            author: Some("芥川".to_string()),
            ..Default::default()
        };
        let books = dbclient.find_books(Some(options)).await.unwrap();
        assert_eq!(books.len(), 100);
        assert_eq!(books[0].book_id.unwrap(), 59016);
        assert_eq!(
            books[0].title.as_ref().unwrap(),
            "現代作家は古典をどうみるか"
        );

        // firstname
        let dbclient = DbClient::new().await.unwrap();
        let options = BookOptions {
            author: Some("独歩".to_string()),
            ..Default::default()
        };
        let books = dbclient.find_books(Some(options)).await.unwrap();
        assert_eq!(books.len(), 45);
        assert_eq!(books[0].book_id.unwrap(), 1044);
        assert_eq!(books[0].title.as_ref().unwrap(), "あの時分");
    }

    #[actix_rt::test]
    async fn fields_filter() {
        let dbclient = DbClient::new().await.unwrap();
        let options = BookOptions {
            title: Some("鼻".to_string()),
            fields: Some(vec!["title".to_string(), "release_date".to_string()]),
            ..Default::default()
        };
        let books = dbclient.find_books(Some(options)).await.unwrap();
        // println!("{:?}", books);
        assert_eq!(books.len(), 2);
        assert_eq!(books[0].title.as_ref().unwrap(), "鼻");
        assert_eq!(
            String::from(books[0].release_date.as_ref().unwrap()),
            "1997-11-04T00:00:00Z"
        );
        assert_eq!(books[1].title.as_ref().unwrap(), "鼻");
        assert_eq!(
            String::from(books[1].release_date.as_ref().unwrap()),
            "1999-01-26T00:00:00Z"
        );

        let dbclient = DbClient::new().await.unwrap();
        let book_id = 123;
        let options = BookOptions {
            fields: Some(vec![
                "card_url".to_string(),
                "html_url".to_string(),
                "title".to_string(),
                "authors".to_string(),
            ]),
            ..Default::default()
        };
        let book = dbclient
            .find_one_book(book_id, Some(options))
            .await
            .unwrap()
            .unwrap();
        // println!("{:?}", book);
        assert!(book.book_id.is_none());
        assert_eq!(book.title.as_ref().unwrap(), "大川の水");
        assert_eq!(
            book.card_url.as_ref().unwrap(),
            "https://www.aozora.gr.jp/cards/000879/card123.html"
        );
    }

    #[actix_rt::test]
    async fn limit_skip_sort() {
        let dbclient = DbClient::new().await.unwrap();
        let options = BookOptions {
            title: Some("/花/".to_string()),
            limit: Some(20),
            skip: Some(100),
            sort: Some(vec![("release_date".to_string(), 1)]),
            ..Default::default()
        };
        let books = dbclient.find_books(Some(options)).await.unwrap();
        assert_eq!(books.len(), 20);
        assert_eq!(books[0].title.as_ref().unwrap(), "白い花");
        assert_eq!(
            String::from(books[0].release_date.as_ref().unwrap()),
            "2008-07-15T00:00:00Z"
        );
    }

    #[actix_rt::test]
    async fn search_after() {
        let dbclient = DbClient::new().await.unwrap();
        let options = BookOptions {
            title: Some("/月/".to_string()),
            limit: Some(50),
            sort: Some(vec![("release_date".to_string(), 1)]),
            after: Some("2009-01-01".to_string()),
            ..Default::default()
        };
        let books = dbclient.find_books(Some(options)).await.unwrap();
        assert_eq!(books.len(), 50);
        assert_eq!(books[0].book_id.unwrap(), 49545);
        assert_eq!(books[0].title.as_ref().unwrap(), "正月の思い出");
        assert_eq!(
            String::from(books[0].release_date.as_ref().unwrap()),
            "2009-01-10T00:00:00Z"
        );
    }
}
